from flask import Flask, render_template, request, redirect,url_for
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime


# set PROJECT_FLASK=app.py

app = Flask(__name__,
    static_url_path='/static')

app.config['SEND_FILE_MAX_AGE_DEFAULT']=0

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///posts.db'
# db = SQLAlchemy(app)


# class BlogPost(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     # Announcements.query.get(id)
#     title = db.Column(db.String(100),nullable=False)
#     content = db.Column(db.Text, nullable=False)
#     author = db.Column(db.String(20),nullable=False, default='N/A' )
#     date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

#     def __repr__(self):
#         return 'Blog Post' + str(self.id)

# all_posts=[
#     {
#         'title': 'post 1',
#         'content': 'This is the content of post 1. lalalal. ',
#         'author':'StemLand'
#     },
#     {
#         'title': 'post 2',
#         'content': 'This is the content of post 1. Lalalal. '

#     }
# ]

@app.route('/')
def index():
    return render_template('dashboard.html')
    # return redirect('/posts')

@app.route('/dash')
def dash():
    return render_template('new_1.html')

# @app.route('/form')
# def form():
#     return render_template('form.html')

# @app.route('/posts',methods=['GET','POST'])
# def posts():
#     print('in posts', request.method)
#     print(url_for('delete',id= 3))
#     if  request.method == 'POST':
#         post_title = request.form['title']
#         post_content = request.form['content']
#         post_author =  request.form['author']
#         new_post= BlogPost(title=post_title, content=post_content, author=post_author)
#         db.session.add(new_post)
#         db.session.commit()
#     return render_template('posts.html', posts1 = BlogPost.query.order_by(BlogPost.date_posted).all()) #
   


# @app.route('/posts/delete/<int:id>')
# def delete(id):
#     post = BlogPost.query.get_or_404(id)
#     db.session.delete(post)
#     db.session.commit()
#     return redirect('/posts')

# 3
# @app.route('/posts/edit/<int:id>',methods=['GET', 'POST'])
# def edit(id):
#     post = BlogPost.query.get_or_404(id)

#     if request.method == 'POST':
#         post.title = request.form['title']
#         post.author = request.form['author']
#         post.content = request.form['content']
#         db.session.commit()
#         return redirect('/posts')
#     else:
#         return render_template('edit.html',post=post)
# # @app.route('/home/users/<string:name>/posts/<int:id>')
# # def hello(name, id):
# #     return "Hello, "+name+",your id is" +str(id)

# # @app.route('/onlyget', methods=['GET'])
# # def get_req():
# #     return 'you can only get this webpage.2'

# @app.route('/posts/new',methods=['GET', 'POST'])
# def new_post():
#     if request.method == 'POST':
#         post.title = request.form['title']
#         post.author = request.form['author']
#         post.content = request.form['content']
#         new_post= BlogPost(title=post_title, content=post_content, author=post_author)
#         db.session.add(new_post)
#         db.session.commit()
#         return redirect('/posts')
#     else:
#         return render_template('new_post.html')



if __name__=="__main__":
    app.run(port=8080,debug=True)
