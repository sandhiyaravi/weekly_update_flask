function create_pie_chart()
{
    
    var ctx = document.getElementById('keywords_pie').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Agriculture Development', 'Financial Services', 'Gates Foundation News', 'Health'],
            datasets: [{
                label: '# of Votes',
                data: [4.08, 23.65, 5.9, 66.37],
                backgroundColor: ['#fbb4ae','#b3cde3','#ccebc5','#decbe4'],
                    
                
                borderColor: ['#fbb4ae','#b3cde3','#ccebc5','#decbe4'],
              
                borderWidth: 1
            }]
        },
        options: {
            plugins: {
                labels: {
                    render:'value'+ '%',

                    precision:2,

                }
            }
        }
    });
}

//To create the stack_chart
function create_stack_chart()
{
    var barChartData = {
        labels: ['Gates Foundation', 'Bill Gates'],
        datasets: [{
            label: 'General',
            backgroundColor:'#f2f2f2',
            data: [30,53.67]
        }, {
            label: 'Doing Good',
            backgroundColor:'#ccebc5',
            data: [50,39.31]
        }, {
            label: 'Doing Evil',
            backgroundColor:'#fbb4ae',
            data: [20,7.02]
        }]

    };
    
    var ctx = document.getElementById('keywords_stack_chart').getContext('2d');
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: 'News - General Perception'
            },
        
            tooltips: {
                mode: 'index',
                intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true,
                }]
            }
        }
    });

}


